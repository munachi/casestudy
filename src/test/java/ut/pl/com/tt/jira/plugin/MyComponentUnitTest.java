package ut.pl.com.tt.jira.plugin;

import org.junit.Test;
import pl.com.tt.jira.plugin.api.MyPluginComponent;
import pl.com.tt.jira.plugin.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}