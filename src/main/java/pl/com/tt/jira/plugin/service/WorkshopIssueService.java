package pl.com.tt.jira.plugin.service;

import pl.com.tt.jira.plugin.bean.IssueData;
import pl.com.tt.jira.plugin.bean.WorkshopIssueBean;
import pl.com.tt.jira.plugin.bean.flight.FlightLogs;


import java.io.IOException;
import java.util.List;


public interface WorkshopIssueService {
    IssueData getIssueDataByIssueKey(String issueKey);
    String createNewIssue(WorkshopIssueBean workshopIssueBean);
    String getAllFlights(String issueKey) throws IOException;
    List<FlightLogs> getFlightLogs();
}
