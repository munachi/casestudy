package pl.com.tt.jira.plugin.bean.flight.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Getter
@Setter
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlightDTO {
    String actualLandingTime;
    String expectedTimeBoarding;
    String flightDirection;
    String flightName;
    Integer flightNumber;
    String gate;
    String scheduleDate;
    String scheduleTime;
}
