package pl.com.tt.jira.plugin.tabpanel;

import com.atlassian.jira.project.Project;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.velocity.VelocityManager;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

@Slf4j
public class WorkshopProjectTabPanel implements WebPanel {

    private final VelocityManager velocityManager;

    @Inject
    public WorkshopProjectTabPanel(@ComponentImport VelocityManager velocityManager) {
        this.velocityManager = velocityManager;
    }

    @Override
    public String getHtml(Map<String, Object> map) {
        Project project = (Project) map.get("project");
        map.put("projectId", project.getId().toString());
        return velocityManager.getBody("templates/", "WorkshopProjectPanel.vm", map);
    }

    @Override
    public void writeHtml(Writer writer, Map<String, Object> map) throws IOException {
        writer.write(getHtml(map));
    }


}
