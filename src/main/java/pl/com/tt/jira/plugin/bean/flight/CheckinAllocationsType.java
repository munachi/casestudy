package pl.com.tt.jira.plugin.bean.flight;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Getter
@Setter
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckinAllocationsType {
    CheckinAllocationType[] checkinAllocations;
    RemarksType remarks;
}
