package pl.com.tt.jira.plugin.bean.flight;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class CheckinClassType {
    String code;
    String description;
}
