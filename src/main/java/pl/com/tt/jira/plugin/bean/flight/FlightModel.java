package pl.com.tt.jira.plugin.bean.flight;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Getter
@Setter
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonAutoDetect
public class FlightModel {
    String lastUpdatedAt;
    String actualLandingTime;
    String actualOffBlockTime;
    String aircraftRegistration;
    AircraftTypeType aircraftType;
    BaggageClaimType baggageClaim;
    CheckinAllocationsType checkinAllocations;
    CodesharesType codeshares;
    String estimatedLandingTime;
    String expectedTimeBoarding;
    String expectedTimeGateClosing;
    String expectedTimeGateOpen;
    String expectedTimeOnBelt;
    String expectedSecurityFilter;
    String flightDirection;
    String flightName;
    Integer flightNumber;
    String gate;
    String pier;
    String id;
    Boolean isOperationalFlight;
    String mainFlight;
    String prefixIATA;
    String prefixICAO;
    Integer airlineCode;
    String publicEstimatedOffBlockTime;
    PublicFlightStateType publicFlightState;
    RouteType route;
    String scheduleDateTime;
    String scheduleDate;
    String scheduleTime;
    String serviceType;
    Integer terminal;
    TransferPositionsType transferPositions;
    String schemaVersion;
}
