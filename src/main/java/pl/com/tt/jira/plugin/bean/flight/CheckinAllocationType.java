package pl.com.tt.jira.plugin.bean.flight;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class CheckinAllocationType {
    String endTime;
    RowsType rows;
    String startTime;
}
