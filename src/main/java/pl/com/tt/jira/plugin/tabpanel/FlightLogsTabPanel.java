package pl.com.tt.jira.plugin.tabpanel;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.velocity.VelocityManager;
import pl.com.tt.jira.plugin.bean.flight.FlightLogs;
import pl.com.tt.jira.plugin.service.WorkshopIssueServiceImpl;

import javax.inject.Inject;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

public class FlightLogsTabPanel implements WebPanel {

    final VelocityManager velocityManager;
    final WorkshopIssueServiceImpl workshopIssueService;

    @Inject
    public FlightLogsTabPanel(@ComponentImport VelocityManager velocityManager, WorkshopIssueServiceImpl workshopIssueService) {
        this.velocityManager = velocityManager;
        this.workshopIssueService = workshopIssueService;
    }

    @Override
    public String getHtml(Map<String, Object> map) {
        final List<FlightLogs> flightLogs = workshopIssueService.getFlightLogs();

        map.put("flightLogs", flightLogs);

        return velocityManager.getBody("templates/", "SearchFlightModule.vm", map);

    }

    @Override
    public void writeHtml(Writer writer, Map<String, Object> map) throws IOException {
        writer.write(getHtml(map));
    }

}
