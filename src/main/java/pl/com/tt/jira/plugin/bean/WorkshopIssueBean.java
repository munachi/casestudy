package pl.com.tt.jira.plugin.bean;

import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonAutoDetect;

@Getter
@Setter
@JsonAutoDetect
public class WorkshopIssueBean {
    private String projectKey;
    private String summary;
    private String description;
    private String issueType;
}
