package pl.com.tt.jira.plugin.bean.flight.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public class ListOfFlightDTO {
    List<FlightDTO> flights;
}
