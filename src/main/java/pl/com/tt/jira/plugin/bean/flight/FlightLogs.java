package pl.com.tt.jira.plugin.bean.flight;


import lombok.*;
import org.codehaus.jackson.annotate.JsonAutoDetect;

@AllArgsConstructor
@JsonAutoDetect
@NoArgsConstructor
@Setter
@Getter
@Builder
public class FlightLogs {
    String user;
    String issueUpdated;
    String timeClicked;

    @Override
    public String toString() {
        return "FlightLogs{" +
                "user='" + user + '\'' +
                ", issueUpdated='" + issueUpdated + '\'' +
                ", timeClicked='" + timeClicked + '\'' +
                '}';
    }
}
