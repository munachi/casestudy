package pl.com.tt.jira.plugin.service;

import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.*;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.codehaus.jackson.map.ObjectMapper;
import pl.com.tt.jira.plugin.bean.IssueData;
import pl.com.tt.jira.plugin.bean.WorkshopIssueBean;
import pl.com.tt.jira.plugin.bean.flight.FlightLogs;
import pl.com.tt.jira.plugin.bean.flight.dto.ListOfFlightDTO;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Named
public class WorkshopIssueServiceImpl implements WorkshopIssueService {

    private final IssueManager issueManager;
    private final IssueFactory issueFactory;
    private final ProjectManager projectManager;
    private final IssueTypeManager issueTypeManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final CustomFieldManager customFieldManager;
    List<FlightLogs> flightLogs = new ArrayList<>();

    @Inject
    public WorkshopIssueServiceImpl(@ComponentImport IssueManager issueManager,
                                    @ComponentImport IssueFactory issueFactory,
                                    @ComponentImport ProjectManager projectManager,
                                    @ComponentImport IssueTypeManager issueTypeManager,
                                    @ComponentImport JiraAuthenticationContext jiraAuthenticationContext,
                                    @ComponentImport CustomFieldManager customFieldManager) {
        this.issueManager = issueManager;
        this.issueFactory = issueFactory;
        this.projectManager = projectManager;
        this.issueTypeManager = issueTypeManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.customFieldManager = customFieldManager;
    }

    @Override
    public IssueData getIssueDataByIssueKey(String issueKey) {
        Issue issue = issueManager.getIssueByCurrentKey(issueKey);
        return new IssueData(issue.getSummary(), issue.getDescription());
    }

    @Override
    public String createNewIssue(WorkshopIssueBean workshopIssueBean) {
        Project project = projectManager.getProjectByCurrentKey(workshopIssueBean.getProjectKey());
        IssueType issueType = issueTypeManager.getIssueTypes().stream()
                .filter(issueTypeFilter -> issueTypeFilter.getName().equals(workshopIssueBean.getIssueType()))
                .findAny().orElse(null);

        if (issueType == null) {
            return "No issue type with corresponding name was found";
        }

        ApplicationUser applicationUser = jiraAuthenticationContext.getLoggedInUser();
        String startDelegationId = "customfield_10000";
        String endDelegationId = "customfield_10001";

        try {
            MutableIssue mutableIssue = issueFactory.getIssue();
            mutableIssue.setSummary(workshopIssueBean.getSummary());
            mutableIssue.setDescription(workshopIssueBean.getDescription());
            mutableIssue.setIssueTypeId(issueType.getId());
            mutableIssue.setReporter(applicationUser);
            mutableIssue.setProjectId(project.getId());
            issueManager.createIssueObject(applicationUser, mutableIssue);
        } catch (CreateException createException) {
            return "There was an error while creating issue object, " + createException.getMessage();
        }

        return "Creating issue was successful!";
    }

    private String formatDate(Instant date){
        return DateTimeFormatter.ISO_LOCAL_DATE.withZone(ZoneId.of("Europe/Warsaw")).format(date);
    }

    private String buildQuery(String startDate, String endDate) {
        return String.format("?fromScheduleDate=%s&toScheduleDate=%s", startDate, endDate);
    }

    public String getAllFlights(String issueKey) {

        Issue issue = issueManager.getIssueByCurrentKey(issueKey);

        String startDelegationId = "customfield_10000";
        String endDelegationId = "customfield_10001";

        CustomField startDelegationCF  = customFieldManager.getCustomFieldObject(startDelegationId);
        CustomField endDelegationCF  = customFieldManager.getCustomFieldObject(endDelegationId);

        Timestamp timestamp1 = (Timestamp) issue.getCustomFieldValue(startDelegationCF);
        Timestamp timestamp2 = (Timestamp) issue.getCustomFieldValue(endDelegationCF);

        String startDelegation = formatDate(Instant.ofEpochMilli(timestamp1.getTime()));
        String endDelegation = formatDate(Instant.ofEpochMilli(timestamp2.getTime()));


        MutableIssue mutableIssue = issueManager.getIssueByKeyIgnoreCase(issueKey);
        ApplicationUser currentUser = jiraAuthenticationContext.getLoggedInUser();

        try{
            StringBuilder sb = new StringBuilder();
            sb.append("https://api.schiphol.nl/public-flights/flights");
            sb.append(buildQuery(startDelegation, endDelegation));

        String urlString = sb.toString();
        URL obj = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("app_id", "3b08b5da");
        connection.setRequestProperty("app_key", "b0202db187614339c26552030f168624");
        connection.setRequestProperty("ResourceVersion", "v4");
        connection.setReadTimeout(50000);

        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) { // success

            ObjectMapper objectMapper = new ObjectMapper();
            ListOfFlightDTO flightList = objectMapper.readValue(connection.getInputStream(), ListOfFlightDTO.class);

            String flight = objectMapper.writer().writeValueAsString(flightList);

            flightLogs.add(FlightLogs.builder()
                    .issueUpdated(issueKey)
                    .timeClicked(LocalDate.now().toString())
                    .user(currentUser.getUsername())
                    .build());

            mutableIssue.setDescription(flight);

            String s = mutableIssue.getDescription();

            issueManager.updateIssue(currentUser, mutableIssue, EventDispatchOption.ISSUE_UPDATED, false);

            return flight;

        } else {
            System.out.println("GET request not worked");
        }

    } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }

    public List<FlightLogs> getFlightLogs(){
        return flightLogs;
    }

}
