package pl.com.tt.jira.plugin.bean.flight;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class RouteType {
    String[] destinations;
    String eu;
    Boolean visa;
}
